ARG CADDY_IMAGE_TAG=2.6.2-alpine
FROM caddy:builder AS builder
RUN xcaddy build --with github.com/techknowlogick/certmagic-s3

ARG CADDY_IMAGE_TAG=${CADDY_IMAGE_TAG}
FROM caddy:${CADDY_IMAGE_TAG}
COPY --from=builder /usr/bin/caddy /usr/bin/caddy

