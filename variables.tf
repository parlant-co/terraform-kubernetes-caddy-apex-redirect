variable "namespace" {
  default = "default"
}

variable "name" {
  default = "apex"
}

variable "service_annotations" {
  type    = map(string)
  default = {}
}

variable "whitelist" {
  type    = list(string)
  default = []
}

variable "acme_email" {}

variable "replicas" {
  type    = number
  default = 1
}

variable "image" {
  value = "registry.gitlab.com/parlant-co/terraform-kubernetes-caddy-apex-redirect:0.0.1"
}

variable "json_server_image" {
  value = "registry.gitlab.com/parlant-co/docker/node-json-server:0.0.1"
}

variable "tolerations" {
  type = list(
    object({
      effect   = string
      key      = string
      value    = optional(string)
      operator = optional(string)
    })
  )
  default = []
}

variable "storage_volume" {
  type = object({
    enabled = boolean
    size    = string
  })
  default = {
    enabled = true
    size    = "1Gi"
  }
}

variable "storage_s3" {
  sensitive = true
  type = object({
    enabled        = boolean
    host           = string
    bucket         = string
    access_key     = string
    secret_key     = string
    prefix         = string
    encryption_key = optional(string)
  })
  default = {
    enabled    = false
    host       = ""
    bucket     = ""
    access_key = ""
    secret_key = ""
    prefix     = ""
  }
}

