locals {
  name = "${var.name}-${random_id.this.hex}"
  selector_labels = {
    "app.kubernetes.io/name"      = "caddy-apex-redirect"
    "app.kubernetes.io/component" = "http-redirect-service"
    "app.kubernetes.io/instance"  = "${var.name}-${random_id.this.hex}"
  }
  labels = merge(local.selector_labels, {
    "app.kubernetes.io/managed-by" = "Terraform"
    "app.kubernetes.io/version"    = var.image
  })

  caddy_config = {
    admin = { disabled = true }
    apps = {
      http = {
        servers = {
          # Note: This Caddy server configuration redirects every request to https://www.<Host>
          "srv0_redirect_static" = {
            listen = [":443", ":80"]
            routes = [
              {
                handle = [
                  {
                    handler = "static_response",
                    headers = {
                      "Location" = [
                        "https://www.{http.request.host}{http.request.uri}"
                      ],
                      "Server" = null
                    },
                    status_code = 301
                  }
                ],
                terminal = true
              }
            ]
            tls_connection_policies = [{}]
          }
        }
      }
      tls = {
        automation = {
          renew_interval = "1h"
          on_demand = {
            rate_limit = {
              interval = "2m"
              burst    = 5
            }
            ask = "http://127.0.0.1:3000/check"
          } # on_demand
          policies = [
            {
              on_demand = true,
              issuers = [
                {
                  module       = "acme",
                  email        = var.acme_email,
                  acme_timeout = "5m"
                },
              ]
            }
          ]
        } # automation
      }   # tls
    }     # apps
    storage = {
      for config in flatten([
        (
          var.storage_s3.enabled ? {
            key = "s3"
            value = {
              "module"         = "s3"
              "host"           = var.storage_s3.host
              "bucket"         = var.storage_s3.bucket
              "access_key"     = var.storage_s3.access_key
              "secret_key"     = var.storage_s3.secret_key
              "prefix"         = lookup(var.storage_s3, "prefix", "")
              "encryption_key" = lookup(var.storage_s3, "encryption_key", "")
            }
          } : []
        ),
        (
          var.storage_volume.enabled ? {
            key = "file_system"
            value = {
              "module" = "file_system"
              "root"   = lookup(var.storage_volume, "root", "")
            }
          } : []
        )
      ]) : config.key => config.value
    } # storage
  }
  domains_json = jsonencode({
    domains = [
      for host in var.whitelist : {
        id = host
      }
    ]
  })
  caddy_json        = jsonencode(local.caddy_config)
  caddy_json_sha256 = sha256(join("", [local.caddy_json, local.domains_json]))
}

resource "random_id" "this" {
  byte_length = 2
}

resource "kubernetes_service_v1" "this" {
  metadata {
    name        = var.name
    namespace   = var.namespace
    annotations = var.service_annotations
  }

  spec {
    type     = "LoadBalancer"
    selector = local.selector_labels
    port {
      port     = 80
      protocol = "TCP"
      name     = "http"
    }
    port {
      port     = 443
      protocol = "TCP"
      name     = "https"
    }
    # port {
    #   port     = 443
    #   protocol = "UDP"
    #   name     = "quic"
    # }
  }
}

resource "kubernetes_stateful_set_v1" "this" {
  metadata {
    name      = var.name
    namespace = var.namespace
    labels    = local.labels
  }

  spec {
    pod_management_policy  = "Parallel"
    replicas               = var.replicas
    revision_history_limit = 5
    sevice_name            = var.name

    selector {
      match_labels = local.selector_labels
    }

    dynamic "vvolume_claim_template" {
      for_each = toset(var.storage_volume.enabled ? ["one"] : [])

      content {
        spec {
          access_modes       = ["ReadWriteOnce"]
          storage_class_name = var.storage_volume.storage_class_name
          resources {
            requests = {
              storage = var.storage_volume.size
            }
          }
        }
      }
    }

    template {
      metadata {
        labels = local.labels
        annotations = {
          "configmap-caddy-json-caddy-json-sha256" = local.caddy_json_sha256
        }
      }

      spec {
        affinity {
          node_affinity {
            required_during_scheduling_ignored_during_execution {
              node_selector_term {
                match_expressions {
                  key      = "dedicated"
                  operator = "In"
                  values   = ["apex"]
                }
              }
            }
          }
          pod_anti_affinity {
            required_during_scheduling_ignored_during_execution {
              topology_key = "kubernetes.io/hostname"
              label_selector {
                match_expressions {
                  key      = "app.kubernetes.io/instance"
                  operator = "In"
                  values   = [local.labels["instance"]]
                }
              }
            }
          }
        }

        termination_grace_period_seconds = 1

        container {
          image = var.json_server_image
          name  = "whitelist"

          command = ["/bin/sh"]
          args = [
            "--host",
            "0.0.0.0",
            "--routes",
            "/configmap/whitelist/routes.json",
            "/configmap/whitelist/domains.json",
          ]

          port {
            container_port = 3000
            name           = "http"
          }

          volume_mount {
            name       = "whitelist"
            mount_path = "/configmap/whitelist"
            read_only  = true
          }
        }

        container {
          image = var.image
          name  = "caddy"

          command = ["caddy"]
          args    = ["run", "--config", "/configmap/caddy-json/caddy.json"]

          port {
            container_port = 80
            name           = "http"
          }

          port {
            container_port = 443
            name           = "https"
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 80
            }
            initial_delay_seconds = 1
            period_seconds        = 3
          }

          resources {
            limits = {
              memory = "256Mi"
            }
          }

          volume_mount {
            name       = "caddy-json"
            mount_path = "/configmap/caddy-json/caddy.json"
            sub_path   = "caddy.json"
            read_only  = true
          }

          dynamic "volume_mount" {
            for_each = toset(var.storage_volume.enabled ? ["one"] : [])

            content {
              name       = "caddy-data"
              mount_path = "/config"
              read_only  = false
              sub_path   = "config"
            }
          }

          dynamic "volume_mount" {
            for_each = toset(var.storage_volume.enabled ? ["one"] : [])

            content {
              name       = "caddy-data"
              mount_path = "/data"
              read_only  = false
              sub_path   = "data"
            }
          }
        }

        dynamic "toleration" {
          for_each = {
            for toleration in var.tolerations :
            "${toleration.key}=${lookup(toleration, "value", "")}:${toleration.effect}(${lookup(toleration, "operator", "")})" => toleration
          }
          content {
            key      = toleration.value.key
            effect   = toleration.value.effect
            value    = lookup(toleration.value, "value", null)
            operator = lookup(toleration.value, "operator", null)
          }
        }

        volume {
          name = "caddy-json"
          config_map {
            name = kubernetes_config_map_v1.this.metadata[0].name
            items {
              key  = "caddy.json"
              path = "caddy.json"
            }
          }
        }

        volume {
          name = "whitelist"
          config_map {
            name = kubernetes_config_map_v1.this.metadata[0].name
            items {
              key  = "domains.json"
              path = "domains.json"
            }
            items {
              key  = "routes.json"
              path = "routes.json"
            }
          }
        }

      }

    }
  }
}

resource "kubernetes_config_map_v1" "this" {
  metadata {
    name      = var.name
    namespace = var.namespace
    labels    = local.labels
  }

  data = {
    "caddy.json"   = local.caddy_json
    "domains.json" = local.domains_json
    "routes.json" = jsonencode({
      "/check\\?domain=:id" = "/domains/:id"
    })
  }
}

